from googleapiclient import discovery

import json


def main():
    yt_api_key = 'INSERT_YOUR_TOKEN_HERE'
    yt = discovery.build('youtube', 'v3', developerKey=yt_api_key)

    videos = []
    page_token = None
    for i in range(100):
        print('Iteration', i)
        resp = yt.search().list(
            q='поездка по России',
            part='snippet', safeSearch='none',
            maxResults=50, pageToken=page_token
        ).execute()
        if 'items' not in resp:
            print('No items in response')
            break
        else:
            videos += resp['items']
        if 'nextPageToken' not in resp:
            print('No next page token in response')
            break
        else:
            page_token = resp['nextPageToken']
    
    with open('videos.json', 'w', encoding='utf8') as json_file:
        json.dump(videos, json_file, ensure_ascii=False)


if __name__ == '__main__':
    main()
