import pandas as pd
import json

pd.set_option('display.max_columns', None)


def main():
    with open('videos.json', 'r', encoding='utf8') as json_file:
        videos = json.load(json_file)
    
    df = pd.json_normalize(videos)
    df.rename(columns=lambda col: col.split('.')[-1], inplace=True)
    df.to_excel('videos.xlsx')


if __name__ == '__main__':
    main()
